<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%@ include file="header.jspf" %>
<title>Huashi applet</title>
<script type="text/javascript">
$(document).ready(function() {
	if (deployJava.versionCheck('<%=minimumVersion%>+')) {
		document.writeln('<body style="background-color:#EEEEEE">');
		runApplet();
		document.writeln('');
		document.writeln('<script type="text/javascript" src="<%=cp%>/applet/js/jquery-1.6.2.min.js"><\/script>');
		document.writeln('<script type="text/javascript" src="<%=cp%>/applet/js/ChartApplet.js"><\/script>');
		document.writeln('</body>');
	}
});
function runApplet() {
	var b=new Browser();
	var w='100%';
	var h='100%';
	if(b.safari || b.chrome){
		h='97%';
	}
	var attributes = {
		name:    '<%=appletName%>',
		code:    '<%=appletClass%>',
		archive: '<%=url%>HuashiApplet.jar,<%=url%>JNative.jar',
		width:   w,
		height:  h
	};
	var parameters = {
		url :'<%=url%>',
		separate_jvm: 	'true'
	};
	var version = '<%=minimumVersion%>';
	deployJava.runApplet(attributes, parameters, version);
}
</script>

</head>
</html>
