var Browser = function() {
	var ua = navigator.userAgent.toLowerCase();
	var s;
	(s = ua.match(/msie ([\d.]+)/)) ? this.ie = s[1] :
	(s = ua.match(/firefox\/([\d.]+)/)) ? this.firefox = s[1] :
	(s = ua.match(/chrome\/([\d.]+)/)) ? this.chrome = s[1] :
	(s = ua.match(/opera.([\d.]+)/)) ? this.opera = s[1] :
	(s = ua.match(/version\/([\d.]+).*safari/)) ? this.safari = s[1] : 0;
};

function getBrowserScale() {
	var browser = new Browser();
	var width = document.body.clientWidth;
	var height = screen.availHeight;
	if(browser.ie){
		if (window.innerWidth){
		    width = window.innerWidth;  
	    }else if((document.body) && (document.body.clientWidth)){
			width = document.body.clientWidth;
		}   
		if(window.innerHeight){
			height = window.innerHeight;
		}else if((document.body) && (document.body.clientHeight)){
			height = document.body.clientHeight;
		}
			 
	    if (document.documentElement  && document.documentElement.clientHeight 
	    		&& document.documentElement.clientWidth){
	    	height = document.documentElement.clientHeight;   
	    	width = document.documentElement.clientWidth;   
		}
	    if (browser.ie == '8.0') {
			width -= 20;
			height -= 30;
			
		} else if (browser.ie == '7.0') {
			width -=20;
			height-= 30;
		} else if (browser.ie == '6.0') {
		} else if (browser.ie == '9.0') {
		}
	}
	else if (browser.firefox) {
		width=self.innerWidth-20;
		height=self.innerHeight-20;
	} else if (browser.firefox == '3.0') {
	} else if (browser.firefox == '5.0') {
	} else if (browser.chrome) {
		width -= 20;
		height = self.innerHeight - 20;
	} else if (browser.safari) {
		width=self.innerWidth-20;
		height=self.innerHeight-20;
	} else if (browser.safari == '5.0') {
	} else {
		width=self.innerWidth-20;
		height=self.innerHeight-20;
	}
	var scale = new Array(2);
	scale[0] = width;
	scale[1] = height;
	return scale;
}

// The minimum scale must be set here
function getAppletScale() {
	var scale = getBrowserScale();
	scale[0] = Math.max(scale[0], 752);//IE752,FF752
	scale[1] = Math.max(scale[1], 460);//IE460,FF505
	return scale;
}

var _resizeTimer;
$(window).resize(function() {
	 if ($('applet')) {
		window.clearTimeout(_resizeTimer);
		_resizeTimer = window.setTimeout('autoResize()', 600);
	} 
});
function autoResize() {
	var scale = getAppletScale();
	$('applet').attr('width', scale[0]);
	$('applet').attr('height', scale[1]);
}
