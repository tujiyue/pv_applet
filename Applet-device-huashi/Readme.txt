1.本工程是特针对华视读卡器进行编写的。
2.目前华视dll文件路径在代码中写死为C:/huashi，所以在该文件夹下要有华视所需的所有dll文件。
3.在java.library.path下要能找到dll文件。
4.目前示例中点“读卡”操作是先初始化端口然后再关闭，这只是一个示例，生产环境中应该只初始化一次。
5.Applet-device-huashi工程的classpath中有一项依赖于服务端Huashi-Applet中的“/Huashi-Applet/WebContent/applet/JNative.jar”