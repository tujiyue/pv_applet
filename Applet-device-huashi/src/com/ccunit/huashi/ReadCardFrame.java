/**
 * 
 */
package com.ccunit.huashi;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
 * @author Bill Tu
 * @since Jul 26, 2012(3:18:50 PM)
 *
 */
public class ReadCardFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7098720983340660704L;
	private JLabel nameLabel=new JLabel("姓名：");
	private JLabel idLabel=new JLabel("身份证号：");
	private JLabel genderLabel=new JLabel("性别：");
	private JLabel birthLabel=new JLabel("出生日期：");
	private JLabel nationLabel=new JLabel("民族：");
	private JLabel departLabel=new JLabel("签发机关：");
	private JLabel validateLabel=new JLabel("有效期限：");
	private JLabel photoLabel=new JLabel("头像：");
	private JLabel addressLabel=new JLabel("地址：");
	private JTextField nameField;
	private JTextField idField;
	private JTextField genderField;
	private JTextField birthField;
	private JTextField nationField;
	private JTextField validateField;
	private JTextField departField;
	private JTextField addressField;
	private JLabel photo = new JLabel();
	private JButton btnRead;
	private ChinaId model;
	private HuashiChinaIdReader reader;
	
	public HuashiChinaIdReader getReader() {
		return reader;
	}

	public void init(){
		reader=new HuashiChinaIdReader();
		nameField=new JTextField(15);
		nameField.setEditable(false);
		idField=new JTextField(30);
		idField.setEditable(false);
		genderField=new JTextField(10);
		genderField.setEditable(false);
		birthField=new JTextField(25);
		birthField.setEditable(false);
		nationField=new JTextField(10);
		nationField.setEditable(false);
		validateField=new JTextField(30);
		validateField.setEditable(false);
		departField=new JTextField(25);
		departField.setEditable(false);
		addressField=new JTextField(30);
		addressField.setEditable(false);
		this.setLayout(new BorderLayout());
		
		JPanel btnPanel=new JPanel();
		btnPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		btnRead=new JButton("读卡");
		
		
		btnPanel.add(btnRead);
		
		JPanel borderCenterPanel=new JPanel();
		borderCenterPanel.setLayout(new BorderLayout());
		
		JPanel topPanel=new JPanel();
		topPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		topPanel.add(nameLabel);
		topPanel.add(nameField);
		topPanel.add(idLabel);
		topPanel.add(idField);
		topPanel.add(genderLabel);
		topPanel.add(genderField);
		
		JPanel centerPanel=new JPanel();
		centerPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		centerPanel.add(birthLabel);
		centerPanel.add(birthField);
		centerPanel.add(nationLabel);
		centerPanel.add(nationField);
		centerPanel.add(departLabel);
		centerPanel.add(departField);
		
		final JPanel bottomPanel=new JPanel();
		bottomPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		bottomPanel.add(validateLabel);
		bottomPanel.add(validateField);
		bottomPanel.add(addressLabel);
		bottomPanel.add(addressField);
		bottomPanel.add(photoLabel);
		bottomPanel.add(photo);
		
		btnRead.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {
				boolean flag=false;
				try{
					flag=reader.initialize();
					if(flag){
						model=reader.read();
						if(null!=model){
							nameField.setText(model.getName());
							idField.setText(model.getIdno());
							genderField.setText(model.getGender()==1?"男":"女");
							birthField.setText(HuashiChinaIdReader.sdf1.format(model.getDateOfBirth()));
							nationField.setText(model.getRace());
							validateField.setText(HuashiChinaIdReader.sdf2.format(model.getEffSince())+"-"+HuashiChinaIdReader.sdf2.format(model.getEffThrough()));
							departField.setText(model.getIssuedBy());
							addressField.setText(model.getAddress());
							photo.setIcon(new ImageIcon(model.getPhoto()));
							photo.repaint();
						}
					}
				}catch(Exception e1){
					System.out.println("error msg:"+e1.getMessage());
					e1.printStackTrace();
				}finally{
					if(!flag){
						JOptionPane.showMessageDialog(null, "读卡初始化失败!");
					}
					if(flag){
						reader.terminate();
					}
				}
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
			
		});
		
		borderCenterPanel.add(btnPanel,BorderLayout.NORTH);
		
		JPanel cp=new JPanel();
		cp.setLayout(new BorderLayout());
		cp.add(topPanel,BorderLayout.NORTH);
		cp.add(centerPanel,BorderLayout.CENTER);
		
		borderCenterPanel.add(cp,BorderLayout.CENTER);
		
		borderCenterPanel.add(bottomPanel,BorderLayout.SOUTH);
		this.add(borderCenterPanel,BorderLayout.NORTH);
		
	}
}
