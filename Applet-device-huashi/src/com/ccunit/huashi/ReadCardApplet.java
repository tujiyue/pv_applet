/**
 * 
 */
package com.ccunit.huashi;

import javax.swing.JApplet;

/**
 * @author Bill Tu
 * @since Jul 26, 2012(4:23:36 PM)
 *
 */
public class ReadCardApplet extends JApplet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6122428056269764190L;
	private ReadCardFrame cardReader;
	@Override
	public void init() {
		cardReader=new ReadCardFrame();
		cardReader.init();
		setContentPane(cardReader.getContentPane());
	}
}
