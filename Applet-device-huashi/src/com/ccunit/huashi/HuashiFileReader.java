/**
 * 
 */
package com.ccunit.huashi;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 * @author Bill Tu
 * @since Jul 21, 2012(11:06:38 AM)
 * 
 */
public final class HuashiFileReader {

	private HuashiFileReader() {

	}

	public static String readFile(String fileName) {
		String output = "";
		File file = new File(fileName);
		if (file.exists()) {
			if (file.isFile()) {
				InputStreamReader ins = null;
				try {
					ins = new InputStreamReader(new FileInputStream(file),
							"GBK");
					BufferedReader input = new BufferedReader(ins);
					StringBuffer buffer = new StringBuffer();
					String text;
					while ((text = input.readLine()) != null)
						buffer.append(text + "&");
					output = buffer.toString();
				} catch (IOException ioException) {
				} finally {
					if (ins != null) {
						try {
							ins.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			} else if (file.isDirectory()) {
				String[] dir = file.list();
				output += "Directory contents:/n";
				for (int i = 0; i < dir.length; i++) {
					output += dir[i] + "/n";
				}
			}
		} else {
		}
		return output;
	}

	public static byte[] readImg(String fileName) {
		byte[] img = new byte[4096];
		File file = new File(fileName);
		if (file.exists()) {
			if (file.isFile()) {
				FileInputStream fis = null;
				try {
					fis = new FileInputStream(file);
					img = new byte[fis.available()];
					fis.read(img);

				} catch (IOException ioException) {
				} finally {
					if (fis != null) {
						try {
							fis.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		return img;
	}

	public static BufferedImage getPhotoBufferedImage() {
		Image img = getPhotoImage();
		BufferedImage bimg = new BufferedImage(img.getWidth(null),
				img.getHeight(null), BufferedImage.TYPE_INT_RGB);
		bimg.createGraphics().drawImage(img, 0, 0, null);
		return bimg;
	}

	/**
	 * 取当前卡中的相片信息
	 * 
	 * @return Image
	 */
	private static Image getPhotoImage() {
		Image img = ParseBMPToJPG.load(HuashiChinaIdReader.FILE_ROOT_DIR
				+ HuashiChinaIdReader.imgFile);
		return img;
	}

	public static void clear(String fileName) {
		File file = new File(fileName);
		if (file.exists() && file.isFile()) {
			file.delete();
		}
	}
}
