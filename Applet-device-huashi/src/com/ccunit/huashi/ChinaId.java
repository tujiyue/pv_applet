/*
 * ChinaId.java
 * created at 4:58:50 PM Jun 10, 2012
 */
package com.ccunit.huashi;

import java.awt.image.BufferedImage;
import java.util.Date;

/**
 * @author Felix Wu
 * @since Jun 10, 2012
 */
public class ChinaId {
	private String name;
	private int gender;
	private String race;
	private Date dateOfBirth;
	private String address;
	private String idno;
	private BufferedImage photo;
	private String issuedBy;
	private Date effSince;
	private Date effThrough;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIdno() {
		return idno;
	}

	public void setIdno(String idno) {
		this.idno = idno;
	}


	public void setPhoto(BufferedImage photo) {
		this.photo = photo;
	}


	public BufferedImage getPhoto() {
		return photo;
	}

	public String getIssuedBy() {
		return issuedBy;
	}

	public void setIssuedBy(String issuedBy) {
		this.issuedBy = issuedBy;
	}

	public Date getEffSince() {
		return effSince;
	}

	public void setEffSince(Date effSince) {
		this.effSince = effSince;
	}

	public Date getEffThrough() {
		return effThrough;
	}

	public void setEffThrough(Date effThrough) {
		this.effThrough = effThrough;
	}

	@Override
	public String toString() {
		StringBuffer sb=new StringBuffer();
		sb.append("name=").append(name);
		sb.append(",race=").append(race);
		sb.append(",gender=").append(gender==1?"男":"女");
		sb.append(",birthday=").append(dateOfBirth);
		sb.append(",issuedBy=").append(issuedBy);
		sb.append(",photo=").append((null!=getPhoto())?"有":"无");
		return sb.toString();
	}
	
	
}
