package com.ccunit.huashi;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.xvolks.jnative.JNative;
import org.xvolks.jnative.Type;


/**
 * @author Bill Tu
 * @since Jul 21, 2012(10:26:13 AM)
 * 
 */
public class HuashiChinaIdReader{

	private static final String TERMB_DLL ="/Termb.dll";
	public static final String txtFile = "/wz.txt";
	public static final String imgFile = "/zp.bmp";
	public static final String FILE_ROOT_DIR ="C:/huashi";
	protected static DateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd");
	protected static DateFormat sdf2 = new SimpleDateFormat("yyyy.MM.dd");
	/**
	 * The constructor.
	 */
	public HuashiChinaIdReader() {
	}
	

	public boolean initialize() {
		// USB
		for (int port = 1001; port <= 1016; port++) {
			if (CVR_InitComm(port) == 1) {
				return true;
			}
		}
		// COM
		for (int port = 0; port <= 5; port++) {
			if (CVR_InitComm(port) == 1) {
				return true;
			}
		}
		return false;
	}

	private int CVR_InitComm(int Port) {
		JNative n = null;
		try {
			System.out.println("java.library.path:"+System.getProperty("java.library.path"));
			n = new JNative(FILE_ROOT_DIR + TERMB_DLL, "CVR_InitComm");
			n.setRetVal(Type.INT);
			n.setParameter(0, Port);
			n.invoke();
			return Integer.parseInt(n.getRetVal());
		} catch (Exception nfe) {
			nfe.printStackTrace(System.out);
			return -1;
		} catch (Error rax) {
			rax.printStackTrace();
			return -1;
		}
	}

	private int CVR_Authenticate() {
		JNative n = null;
		try {
			n = new JNative(FILE_ROOT_DIR + TERMB_DLL, "CVR_Authenticate");
			n.setRetVal(Type.INT);
			n.invoke();
			return Integer.parseInt(n.getRetVal());
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
			return -1;
		} catch (Error rax) {
			return -1;
		} 
	}

	private int CVR_CloseComm() {
		JNative n = null;
		try {
			n = new JNative(FILE_ROOT_DIR + TERMB_DLL, "CVR_CloseComm");
			n.setRetVal(Type.INT);
			n.invoke();
			return Integer.parseInt(n.getRetVal());
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
			return -1;
		} catch (Error rax) {
			return -1;
		} finally {
		}
	}

	private int CVR_Read_Content(int Active) {
		JNative n = null;
		try {
			n = new JNative(FILE_ROOT_DIR + TERMB_DLL, "CVR_Read_Content");
			n.setRetVal(Type.INT);
			n.setParameter(0, Active);
			n.invoke();
			return Integer.parseInt(n.getRetVal());
		} catch (Exception ex) {
			ex.printStackTrace(System.out);
			return -1;
		} catch (Error rax) {
			return -1;
		} finally {
		}
	}

	public boolean terminate() {
		int rntCode = CVR_CloseComm();
		if (rntCode > -1) {
			return true;
		}
		return false;
	}

	public ChinaId read() {
		boolean readSuccess = readAndSaveToFile();
		if (readSuccess) {
			return readFromFile();
		}
		return null;
	}

	private synchronized ChinaId readFromFile() {
		ChinaId cid = new ChinaId();
		String str[] = HuashiFileReader.readFile(FILE_ROOT_DIR + txtFile).split(
				"[&年月日]");
		cid.setAddress(trim(str[7]));
		try {
			String birthday = trim(str[3]) + "/" + trim(str[4]) + "/"
					+ trim(str[5]);
			cid.setDateOfBirth(sdf1.parse(birthday));
			cid.setEffSince(sdf2.parse(trim(str[10]).split("-")[0]));
			cid.setEffThrough(sdf2.parse(trim(str[10]).split("-")[1]));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		cid.setGender(trim(str[1]).equals("男") ? 1 : 2);
		cid.setIdno(trim(str[8]));
		cid.setIssuedBy(trim(str[9]));
		cid.setName(trim(str[0]));
		cid.setRace(trim(str[2]));
		cid.setPhoto(HuashiFileReader.getPhotoBufferedImage());
		File theFile = new File(FILE_ROOT_DIR + imgFile);
		if (theFile.exists())
			theFile.delete();
		theFile = new File(FILE_ROOT_DIR + txtFile);
		if (theFile.exists())
			theFile.delete();
		return cid;
	}

	private String trim(String str) {
		if (str != null) {
			return str.trim();
		} else {
			return null;
		}
	}

	private boolean readAndSaveToFile() {
		if (CVR_Authenticate() != -1) {
			int contentCode = CVR_Read_Content(4);
			if (contentCode == 1) {
				return true;
			}
		}
		return false;
	}
}
